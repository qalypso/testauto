# jenkins@307c6017e578:~$ cat /etc/os-release                                                                                                                                           
# PRETTY_NAME="Debian GNU/Linux 9 (stretch)"
# NAME="Debian GNU/Linux"
# VERSION_ID="9"
# VERSION="9 (stretch)"
# ID=debian
# HOME_URL="https://www.debian.org/"
# SUPPORT_URL="https://www.debian.org/support"
# BUG_REPORT_URL="https://bugs.debian.org/"
# jenkins@307c6017e578:~$ uname -a
# Linux 307c6017e578 4.19.76-linuxkit #1 SMP Thu Oct 17 19:31:58 UTC 2019 x86_64 GNU/Linux
# jenkins@307c6017e578:~$ 
# jenkins@307c6017e578:~$ cat /etc/version
# cat: /etc/version: No such file or directory
# jenkins@307c6017e578:~$ cat /etc/issue                                                                                                                                                
# Debian GNU/Linux 9 \n \l
FROM jenkins/jnlp-slave:3.27-1

USER root

RUN apt-get update
# RUN apt-get install curl htop git zip nano ncdu build-essential chrpath libssl-dev libxft-dev pkg-config glib2.0-dev libexpat1-dev gobject-introspection python-gi-dev apt-transport-https libgirepository1.0-dev libtiff5-dev libjpeg-turbo8-dev libgsf-1-dev fail2ban nginx -y

# RUN apt-get install build-essential
RUN apt-get install -y software-properties-common build-essential zlib1g-dev libncurses5-dev libgdbm-dev libnss3-dev libssl-dev libreadline-dev libffi-dev wget

# need postgres driver
# RUN deb http://apt.postgresql.org/pub/repos/apt/ 9-pgdg main
# RUN wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -
# RUN apt-get update
# RUN apt-get install -y postgresql-client
# RUN apt-get install -y postgresql-client-12

# Install Node.js
RUN curl -sL https://deb.nodesource.com/setup_13.x | bash
RUN apt-get install --yes nodejs
RUN node -v
RUN npm -v
RUN npm i -g nodemon
RUN nodemon -v

# Install Newman
RUN npm install -g newman
RUN npm install -g newman-reporter-htmlextra

# Install python 3.81
# RUN add-apt-repository ppa:deadsnakes/ppa
# RUN apt-get update
# RUN apt-get install -y python3.8

RUN curl -O https://www.python.org/ftp/python/3.8.1/Python-3.8.1.tar.xz
RUN tar -xf Python-3.8.1.tar.xz

RUN cd Python-3.8.1 && ./configure --enable-optimizations
RUN cd Python-3.8.1 && make 
RUN cd Python-3.8.1 && make altinstall

RUN python3.8 -V
RUN echo "alias python=/usr/local/bin/python3.8" >> ~/.bashrc
RUN alias python=/usr/local/bin/python3.8
RUN python -V
# RUN apt-get install -y python3-pip
# RUN pip3 install pipenv
# RUN alias python=python3.8 && python install pip && pip install pipenv
# RUN python install -m pip3

RUN python3.8 -m pip install pipenv

# RUN pipenv install --system selenium==4.0.0a3 --skip-lock
# RUN pipenv install --system pytest --skip-lock
# RUN pipenv install --system pytest-html --skip-lock
# RUN pipenv install --system allure-pytest --skip-lock
# RUN pipenv install --system pytest-selenium --skip-lock
# RUN pipenv install --system pytest-xdist --skip-lock
# RUN pipenv install --system pytest-sugar --skip-lock
# RUN pipenv install --system pytest-variables --skip-lock

RUN python3.8 -m pip install selenium==4.0.0a5
RUN python3.8 -m pip install pytest
RUN python3.8 -m pip install pytest-html
RUN python3.8 -m pip install allure-pytest
RUN python3.8 -m pip install pytest-selenium
RUN python3.8 -m pip install pytest-xdist
# RUN python3.8 -m pip install pytest-sugar
RUN python3.8 -m pip install pytest-variables
RUN python3.8 -m pip install tavern


RUN apt-get install -y postgresql
RUN apt-get install libpq-dev
RUN python3.8 -m pip install psycopg2

# Define default command.
# CMD ["bash"]

ENV JMETER_VERSION 5.2.1
ENV JMETER_HOME /opt/apache-jmeter-${JMETER_VERSION}
ENV JMETER_BIN $JMETER_HOME/bin
ENV	JMETER_DOWNLOAD_URL https://archive.apache.org/dist/jmeter/binaries/apache-jmeter-${JMETER_VERSION}.tgz

RUN mkdir -p /tmp/dependencies

RUN curl -L --silent ${JMETER_DOWNLOAD_URL} >  /tmp/dependencies/apache-jmeter-${JMETER_VERSION}.tgz  \
&& mkdir -p /opt  \
&& tar -xzf /tmp/dependencies/apache-jmeter-${JMETER_VERSION}.tgz -C /opt  \
&& rm -rf /tmp/dependencies

# TODO: plugins (later)
# && unzip -oq "/tmp/dependencies/JMeterPlugins-*.zip" -d $JMETER_HOME

# Set global PATH such that "jmeter" command is found
ENV PATH $PATH:$JMETER_BIN

# Entrypoint has same signature as "jmeter" command
# COPY entrypoint.sh /


#CHROMEDRIVER
# Install the latest versions of Google Chrome and Chromedriver:
RUN export DEBIAN_FRONTEND=noninteractive \
  && apt-get update \
  && apt-get install --no-install-recommends --no-install-suggests -y \
    unzip \
    gnupg \
  && GOOGLE_LINUX_DL=https://dl.google.com/linux \
  && curl -sL "$GOOGLE_LINUX_DL/linux_signing_key.pub" | apt-key add - \
  && curl -sL "$GOOGLE_LINUX_DL/direct/google-chrome-stable_current_amd64.deb" \
    > /tmp/chrome.deb \
  && apt install --no-install-recommends --no-install-suggests -y \
    /tmp/chrome.deb \
  && CHROMIUM_FLAGS='--no-sandbox --disable-dev-shm-usage' \
  # Patch Chrome launch script and append CHROMIUM_FLAGS to the last line:
  && sed -i '${s/$/'" $CHROMIUM_FLAGS"'/}' /opt/google/chrome/google-chrome \
  && BASE_URL=https://chromedriver.storage.googleapis.com \
  && VERSION=$(curl -sL "$BASE_URL/LATEST_RELEASE") \
  && curl -sL "$BASE_URL/$VERSION/chromedriver_linux64.zip" -o /tmp/driver.zip \
  && unzip /tmp/driver.zip \
  && chmod 755 chromedriver \
  && mv chromedriver /usr/local/bin/ \
  # Remove obsolete files:
  && apt-get autoremove --purge -y \
    unzip \
    gnupg \
  && apt-get clean \
  && rm -rf \
    /tmp/* \
    /usr/share/doc/* \
    /var/cache/* \
    /var/lib/apt/lists/* \
    /var/tmp/*

# Set the Chrome repo.
RUN apt-get update && apt-get install -y gnupg2
RUN wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | apt-key add - \
    && echo "deb http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google.list
# Install Chrome.
RUN apt-get update && apt-get -y install google-chrome-stable

#Firefox
RUN wget -O ~/FirefoxSetup.tar.bz2 "https://download.mozilla.org/?product=firefox-latest&os=linux64"
RUN tar xjf ~/FirefoxSetup.tar.bz2 && rm ~/FirefoxSetup.tar.bz2 \
    && mv firefox /opt/firefox \
    && mkdir -p /usr/bin/firefox \
    && ln -s /opt/firefox/firefox-bin /usr/bin/firefox
# Geckodriver
RUN wget https://github.com/mozilla/geckodriver/releases/download/v0.26.0/geckodriver-v0.26.0-linux64.tar.gz
RUN tar -xvzf geckodriver* \
    && chmod +x geckodriver && mv geckodriver /usr/bin/

# Install XVFB
RUN apt-get update && apt-get install -y xvfb


EXPOSE 4444

RUN apt-get install xsltproc
RUN apt-get install -y zip


# RUN wget http://selenium-release.storage.googleapis.com/4.0-alpha5/selenium-server-4.0.0-alpha-5.jar
# RUN ls -lia
# RUN java -jar selenium-server-4.0.0-alpha-5.jar hub &

# # Install Firefox
# RUN apt-get install -y firefox

# # Install Gecko Driver
# ENV GECKODRIVER_VERSION 0.26.0
# RUN wget --no-verbose -O /tmp/geckodriver.tar.gz https://github.com/mozilla/geckodriver/releases/download/v$GECKODRIVER_VERSION/geckodriver-v$GECKODRIVER_VERSION-linux64.tar.gz \
#   && rm -rf /opt/geckodriver \
#   && tar -C /opt -zxf /tmp/geckodriver.tar.gz \
#   && rm /tmp/geckodriver.tar.gz \
#   && mv /opt/geckodriver /opt/geckodriver-$GECKODRIVER_VERSION \
#   && chmod 755 /opt/geckodriver-$GECKODRIVER_VERSION \
#   && ln -fs /opt/geckodriver-$GECKODRIVER_VERSION /usr/bin/geckodriver \
#   && ln -fs /opt/geckodriver-$GECKODRIVER_VERSION /usr/bin/wires

# # Install XVFB
# RUN apt-get update && apt-get install -y xvfb